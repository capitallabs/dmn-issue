package org.camunda.app;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DmnThreadingTest {
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	final String url = "/process-definition/key/process/start";

	@Test
	void testMultiThreading() throws IOException {
		String payload = IOUtils.toString( DmnThreadingTest.class.getClassLoader().getResourceAsStream("payload.json") );
		List<String> instances = Lists.list(payload, payload, payload, payload, payload, payload, payload, payload, payload, payload, payload);
		instances.parallelStream().forEach(pl -> start(pl));
	}

	@Test
	void testSingleThreading() throws IOException {
		String payload = IOUtils.toString( DmnThreadingTest.class.getClassLoader().getResourceAsStream("payload.json") );
		List<String> instances = Lists.list(payload, payload, payload, payload, payload, payload, payload, payload, payload, payload, payload);
		instances.stream().forEach(pl -> start(pl));
	}
	
	void start(String pl) {
        final HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.add(
        	HttpHeaders.AUTHORIZATION,
        	"Basic " + Base64.getEncoder().encodeToString(("demo:demo").getBytes(StandardCharsets.UTF_8))
        );
        
        reqHeaders.add(HttpHeaders.CONTENT_TYPE, "application/json");
        final HttpEntity<String> entity = new HttpEntity<String>(pl, reqHeaders);
        assertTrue(this.restTemplate.exchange(
        	"http://localhost:"+port+"/rest"+url,
        	HttpMethod.POST,
        	entity,
        	String.class
        ).getStatusCode().is2xxSuccessful());
	}
}
