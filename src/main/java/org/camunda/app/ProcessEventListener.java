package org.camunda.app;

import java.util.HashMap;
import java.util.Map;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ProcessEventListener {

	Map<String, Map<String, String>> storage = new HashMap<>();
	
	@EventListener
	public void onExecutionEvent(DelegateExecution execution) {
		
		if( "end".equals(execution.getEventName()) ) {
			
			Map<String, String> vars = new HashMap<>();

			execution.getVariableNames().forEach(vname->{
				var value = execution.getVariable(vname);
				vars.put(vname, value == null ? null : value.toString());
			});
			
			storage.put(execution.getActivityInstanceId(), vars);
		}
	}
}
