package org.camunda.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@Autowired
	private ProcessEventListener listener;
	
	@GetMapping("/api/v1/testing/{id}/variables")
	public ResponseEntity<?> getVariables( @PathVariable("id") String activityId ){
		return ResponseEntity.ok(listener.storage.get(activityId));
	}
	
}
